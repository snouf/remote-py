#! /usr/bin/python

#Denon
from remote import denon
dn=denon('192.168.0.30')    # IP of denon device
dn.push('SITUNER')          # command denon protocol serial

#Freebox
from remote import freebox
fbx=freebox(31539609)       # remote code (available in freeboxplayer remote preferences)
fbx.push('1')               # key

#Freebox
from remote import samsung
smg=samsung('192.168.0.16') # IP of tv
smg.push('KEY_1')           # key code
