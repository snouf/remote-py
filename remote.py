#! /usr/bin/python

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


from telnetlib import Telnet
from urllib.request import urlopen
from time import sleep
import socket, base64, logging, netifaces


def get_ip(ip_dest):
    '''
        Return remote ip
    '''
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect((ip_dest, 0))
    return s.getsockname()[0].encode()


def get_mac(ip):
    '''
        Return remote mac adress
    '''
    for i in netifaces.interfaces():
        addrs = netifaces.ifaddresses(i)
        try:
            if_mac = addrs[netifaces.AF_LINK][0]['addr']
            if_ip = addrs[netifaces.AF_INET][0]['addr'].encode()
        except:# IndexError, KeyError: #ignore ifaces that dont have MAC or IP
            if_mac = if_ip = None
        if if_ip == ip:
            return if_mac.encode()
    return None



#########
# - D - #
#########

class denon:
    def __init__(self,host) :
        '''
            connect on denon device
             - host (STR): denon hostname or IP
        '''
        self.tn = Telnet(host)

    def push(self,cmd) :
        '''
            Push a command.
             - cmd (STR): a command (see denon serial protocol)
        '''
        self.tn.write(cmd.encode() + b"\r")


#########
# - F - #
#########

class freebox:
    def __init__(self,code, player_num=1) :
        '''
            connect on freebox player
             - code (STR or INT): remote code (available in freeboxplayer remote preferences)
             - player_num=1 (STR or INT): if you have 2 player for second use 2
        '''
        self._host = 'hd%s.freebox.fr'%player_num
        self._code = code

    def push(self,key,long_press=False) :
        '''
            Push a key of remote
             - key (STR): remote key ("1", "2", "power", "red" ... see readme for other)
             - long_press=False (BOOL): long press
        '''
        urlopen("http://%s/pub/remote_control?code=%s&key=%s&long=%i"%(self._host,self._code,key,long_press))


#########
# - S - #
#########

class samsung:
    def __init__(self,host,name='python remote') :
        '''
            Connect on samsung TV
             - host (STR): TV hostname or IP
             - name='python remote' (STR): remote name
        '''
        self.host = host.encode()
        self.name = name.encode()
        self.src = get_ip(self.host)
        self.mac = get_mac(self.src)


    def push(self,key):
        '''
            Push a key of remote ()
             - key: remote key ("KEY_1", "KEY_2" ...) see http://openremote.org/display/docs/OpenRemote+2.0+How+To+-+Samsung+TV+Remote
        '''
        key = key.encode()
        new = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        new.connect((self.host, 55000))
        msg = b'\x64' + b'\x00' +\
            bytes([len(base64.b64encode(self.src))]) + b'\x00' + base64.b64encode(self.src) +\
            bytes([len(base64.b64encode(self.mac))]) + b'\x00' + base64.b64encode(self.mac) +\
            bytes([len(base64.b64encode(self.name))]) + b'\x00' + base64.b64encode(self.name)
        pkt = b'\x00' +\
            bytes([len(self.name)]) + b'\x00' + self.name +\
            bytes([len(msg)]) + b'\x00' + msg
        new.send(pkt)
        msg = b'\x00\x00\x00' +\
            bytes([len(base64.b64encode(key))]) + b'\x00' + base64.b64encode(key)
        pkt = b'\x00' +\
            bytes([len(self.host)])  + b'\x00' + self.host +\
            bytes([len(msg)]) + b'\x00' + msg
        new.send(pkt)
        new.close()
        sleep(0.1)

